package prekes;

import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/prekes/prekes.txt";
        String rezFailas = new File("").getAbsolutePath()
                + "/src/prekes/rezultatai.txt";
        List<Preke> visosPrekes = skaityti(failoKelias);
        String options = "1 veiksmas: Rasti prekę, kurios vienetų turi mažiausiai ir ją atspausdinti konsolėje;\n" +
                "2 veiksmas: Rasti prekę, kurios vienetų turi daugiausiai ir ją atspausdinti konsolėje;\n" +
                "3 veiksmas: Išfiltruoti prekes pagal tipą ir atspausdinti į failą atfiltruotosPrekes.txt;\n" +
                "4 veiksmas: Pridėti prekę prie esančio sąrašo ir tą sąrašą išsaugoti prekes.txt faile;\n" +
                "5 veiksmas: Ištrinti prekę pagal prekės id ir tą sąrašą išsaugoti prekes.txt faile;\n" +
                "6 veiksmas: Redaguoti prekę pagal prekės id ir tą sąrašą išsaugoti prekes.txt faile;";
        selectOption((ArrayList<Preke>) visosPrekes, options, failoKelias, rezFailas);
    }

    public static void pakeistiPreke(List<Preke> visosPrekes) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/prekes/prekes.txt";
        System.out.println("Iveskite skaiciu nuo 1 iki 4 kokio tipo preke norite pakeisti. Galimi variantai: \n" +
                "1. Televizorius 2. Nesiojamas-kompiuteris 3. Skalbykle 4. Dziovykle ");
        String tipas = koksTipas();
        List<Integer> idListas = pakeistiPrekeIdList(visosPrekes, tipas);
        Scanner scanner = new Scanner(System.in);
        Integer idToCheck = scanner.nextInt();
        System.out.println("Iveskite prekes pavadinima.");
        String pavadinimas = scanner.next();
        Integer kiekis = skaicius();
        Double kaina = skaiciusDouble();
        if (idListas.contains(idToCheck)) {
            for (Preke visosPreke : visosPrekes) {
                Boolean checkId = visosPreke.getId().equals(idToCheck);
                String tipasTikrinti = visosPreke.getTipas();
                if (tipasTikrinti.equals("televizoriai") && checkId) {
                    visosPreke.setPavadinimas(pavadinimas);
                    visosPreke.setKiekis(kiekis);
                    visosPreke.setKaina(kaina);
                    System.out.println("Iveskite prekes technologija.");
                    String technologija = scanner.next();
                    visosPreke.setTechnologija(technologija);
                    System.out.println("Iveskite prekes raiska.");
                    String raiska = scanner.next();
                    visosPreke.setRaiska(raiska);
                } else if (tipasTikrinti.equals("nesiojamas-kompiuteris") && checkId) {
                    visosPreke.setPavadinimas(pavadinimas);
                    visosPreke.setKiekis(kiekis);
                    visosPreke.setKaina(kaina);
                    System.out.println("Iveskite nesiojamojo kompiuterio procesoriaus tipa.");
                    String procas = scanner.next();
                    visosPreke.setProcesiorius(procas);
                    System.out.println("Iveskite nesiojamojo kompiuterio RAM atminties kieki");
                    String ram = scanner.next();
                    visosPreke.setRamAtmintis(ram);
                    System.out.println("Iveskite nesiojamojo kompiuterio disko dydi");
                    String diskoTalpa = scanner.next();
                    visosPreke.setDiskoTalpa(diskoTalpa);
                } else if (tipasTikrinti.equals("skalbykle") && checkId) {
                    visosPreke.setPavadinimas(pavadinimas);
                    visosPreke.setKiekis(kiekis);
                    visosPreke.setKaina(kaina);
                    System.out.println("Iveskite skalbimo masinos bugno talpa");
                    String talpa = scanner.next();
                    visosPreke.setTalpa(talpa);
                } else if (tipasTikrinti.equals("dziovykle") && checkId) {
                    visosPreke.setPavadinimas(pavadinimas);
                    visosPreke.setKiekis(kiekis);
                    visosPreke.setKaina(kaina);
                    System.out.println("Iveskite dziovykles bugno talpa");
                    String talpa = scanner.next();
                    visosPreke.setTalpa(talpa);
                }
            }
            rasyti(failoKelias, visosPrekes);
            System.out.println("Preke pakeista!!!");
        } else {
            System.out.println("Preke nerasta!!!");
        }

    }


    public static Integer skaicius() {
        Scanner scanner = new Scanner(System.in);
        Integer kiekis;
        do {
            System.out.println("Iveskite prekes kieki. Iveskite teigiama skaiciu, aciu!");
            while (!scanner.hasNextInt()) {
                System.out.println("Tai ne skaicius!");
                scanner.next();
            }
            kiekis = scanner.nextInt();
        } while (kiekis <= 0);
        return kiekis;
    }

    public static Integer skaicius1() {
        Scanner scanner = new Scanner(System.in);
        Integer kiekis;
        do {
            while (!scanner.hasNextInt()) {
                System.out.println("Tai ne skaicius !");
                scanner.next();
            }
            kiekis = scanner.nextInt();

        } while (kiekis <= 0 || kiekis > 4);
        return kiekis;
    }

    public static Double skaiciusDouble() {
        Scanner scanner = new Scanner(System.in);
        Double kiekis;
        do {
            System.out.println("Iveskite prekes kaina  aciu!");
            while (!scanner.hasNextDouble()) {
                System.out.println("Tai ne skaicius!");
                scanner.next();
            }
            kiekis = scanner.nextDouble();
        } while (kiekis <= 0);
        return kiekis;
    }

    public static void pridetiPreke(ArrayList<Preke> prekes, String failoKelias) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Iveskite skaiciu nuo 1 iki 4 kokio tipo preke norite prideti." + '\n' +
                "1. Televizorius 2. Nesiojamas-kompiuteris 3. Skalbykle 4. Dziovykle ");
        String tipas = koksTipas();
        System.out.println("Iveskite prekes pavadinima.");
        String pavadinimas = scanner.nextLine();
        Integer kiekis = skaicius();
        Double kaina = skaiciusDouble();
        if (tipas.equals("televizoriai")) {
            System.out.println("Iveskite prekes technologija.");
            String technologija = scanner.nextLine();
            System.out.println("Iveskite prekes raiska.");
            String raiska = scanner.nextLine();
            Preke telikas = new Televizorius(prekes.get(prekes.size() - 1).getId() + 1, tipas,
                    pavadinimas, kiekis, kaina, technologija, raiska);
            prekes.add(telikas);
            System.out.println("Prideta preke " + prekes.get(prekes.size() - 1));

        } else if (tipas.equals("nesiojamas-kompiuteris")) {
            System.out.println("Iveskite nesiojamojo kompiuterio procesoriaus tipa.");
            String procas = scanner.next();
            System.out.println("Iveskite nesiojamojo kompiuterio RAM atminties kieki");
            String ram = scanner.next();
            System.out.println("Iveskite nesiojamojo kompiuterio disko dydi");
            String diskoTalpa = scanner.next();
            Preke kompas = new Kompas(prekes.get(prekes.size() - 1).getId() + 1, tipas,
                    pavadinimas, kiekis, kaina, procas, ram, diskoTalpa);
            prekes.add(kompas);
            System.out.println("Prideta preke " + prekes.get(prekes.size() - 1));
        } else if (tipas.equals("skalbykle")) {
            System.out.println("Iveskite skalbimo masinos bugno talpa");
            String talpa1 = scanner.next();
            Preke skalbykle = new Skalbykle(prekes.get(prekes.size() - 1).getId() + 1, tipas,
                    pavadinimas, kiekis, kaina, talpa1);
            prekes.add(skalbykle);
            System.out.println("Prideta preke " + prekes.get(prekes.size() - 1));
        } else if (tipas.equals("dziovykle")) {
            System.out.println("Iveskite skalbimo masinos bugno talpa");
            String talpa = scanner.next();
            Preke dziovykle = new Dziovykle(prekes.get(prekes.size() - 1).getId() + 1, tipas,
                    pavadinimas, kiekis, kaina, talpa);
            prekes.add(dziovykle);
            System.out.println("Prideta preke " + prekes.get(prekes.size() - 1));
        }
        rasyti(failoKelias, prekes);

    }

    public static String koksTipas() {
        String tipas = null;
        Integer tipasSk = skaicius1();
        if (tipasSk.equals(1)) {
            tipas = "televizoriai";
        } else if (tipasSk.equals(2)) {
            tipas = "nesiojamas-kompiuteris";
        } else if (tipasSk.equals(3)) {
            tipas = "skalbykle";
        } else if (tipasSk.equals(4)) {
            tipas = "dziovykle";
        }
        return tipas;
    }

    public static List<Integer> salintiPrekeIdList(List<Preke> prekes) {
        List<Integer> idTrinti = new ArrayList<>();
        String tipas = koksTipas();
        for (Preke preke : prekes) {
            if (preke.getTipas().equals(tipas)) {
                idTrinti.add(preke.getId());
                System.out.println(preke);
            }
        }
        System.out.println("Galite istrinti sias prekes, iveskite prekes id numeri, kuria norite istrinti");
        System.out.println("//////////////////////////////////////////////////////////////////////////////");
        return idTrinti;
    }

    public static List<Integer> pakeistiPrekeIdList(List<Preke> prekes, String tipas) {
        List<Integer> idTrinti = new ArrayList<>();
        for (int i = 0; i < prekes.size(); i++) {
            if (prekes.get(i).getTipas().equals(tipas)) {
                idTrinti.add(prekes.get(i).getId());
                System.out.println(prekes.get(i));
            }
        }
        System.out.println("Galite pakeisti sias prekes, iveskite prekes id numeri, kuria norite pakeisti");
        System.out.println("//////////////////////////////////////////////////////////////////////////////");
        return idTrinti;
    }

    public static void salintiPrekepagalId(List<Preke> prekes) {
        String failoKelias = new File("").getAbsolutePath()
                + "/src/prekes/prekes.txt";
        System.out.println("Iveskite skaiciu nuo 1 iki 4 kokio tipo preke norite istrinti. Galimi variantai: \n" +
                "1. Televizorius 2. Nesiojamas-kompiuteris 3. Skalbykle 4. Dziovykle ");
        List<Integer> idListas = salintiPrekeIdList(prekes);
        Scanner scanner = new Scanner(System.in);
        Integer idToCheck = scanner.nextInt();
        if (idListas.contains(idToCheck)) {
            prekes.removeIf(e -> e.getId().equals(idToCheck));
            rasyti(failoKelias, prekes);
            System.out.println("Preke istrinta!!!");
        } else {
            System.out.println("Preke nerasta!!!");
        }
    }

    public static void atfiltruotosPrekes(List<Preke> list, String rezultatuFailoPath) {
        System.out.println("Iveskite skaiciu nuo 1 iki 4 kokio tipo prekes norite atspausdinti i faila. Galimi variantai: \n" +
                "1. Televizorius 2. Nesiojamas-kompiuteris 3. Skalbykle 4. Dziovykle ");
        String tipas = koksTipas();
        List<Preke> atfiltruotos = new ArrayList<>();
        for (Preke preke : list) {
            if (preke.getTipas().equals(tipas)) {
                atfiltruotos.add(preke);
            }
        }
        rasyti(rezultatuFailoPath, atfiltruotos);
    }

    public static void maxPrekiukiekis(List<Preke> list) {
        Integer min = Integer.MIN_VALUE;
        Integer ind = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getKiekis() > min) {
                min = list.get(i).getKiekis();
                ind = i;
            }
        }
        System.out.println("////////////////////////////////////////////////////////////////////////////////////////");
        System.out.println("Didziausia kieki turinti preke yra : " + list.get(ind));
    }

    public static void minPrekiukiekis(List<Preke> list) {
        Integer max = Integer.MAX_VALUE;
        Integer ind = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getKiekis() < max) {
                max = list.get(i).getKiekis();
                ind = i;
            }
        }
        System.out.println("////////////////////////////////////////////////////////////////////////////////////////");
        System.out.println("Maziausia kieki turinti preke yra : " + list.get(ind));
    }

    public static void maxPrekiu(List<Preke> list) {
        Map<String, Integer> count = new HashMap<>();
        for (Preke word : list) {  /* Counts the quantity of each
                                     element */
            if (!count.containsKey(word.getTipas())) {
                count.put(word.getTipas(), 1);
            } else {
                int value = count.get(word.getTipas());
                value++;

                count.put(word.getTipas(), value);
            }
        }

        System.out.println("////////////////////////////////////////////////////////////////////////////////////////");
        System.out.println("Daugiausiai sio tipo prekiu: ");
        System.out.println(Collections.max(count.entrySet(),
                Comparator.comparingInt(Map.Entry::getValue)).getKey() + " " + Collections.max(count.values()));
    }

    public static void minPrekiu(List<Preke> list) {
        Map<String, Integer> count = new HashMap<>();

        for (Preke word : list) {  /* Counts the quantity of each
                                      element */
            if (!count.containsKey(word.getTipas())) {
                count.put(word.getTipas(), 1);
            } else {
                int value = count.get(word.getTipas());
                value++;

                count.put(word.getTipas(), value);
            }
        }
        List<Preke> lessCommons = new ArrayList<>(); /* Max elements  */

        for (Map.Entry<String, Integer> e : count.entrySet()) {

            if (e.getValue().equals(Collections.min(count.values()))) {
                for (Preke preke : list) {
                    if (preke.getTipas().equals(e.getKey())) {
                        lessCommons.add(preke);
                    }
                }
            }
        }
        System.out.println("////////////////////////////////////////////////////////////////////////////////////////");
        System.out.println("Maziausiai yra sio tipo prekiu: ");
        System.out.println(lessCommons);
        System.out.println("////////////////////////////////////////////////////////////////////////////////////////");
    }

    public static void clear() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void selectOption(ArrayList<Preke> visosPrekes, String options, String failas, String rezFailas) {
        skaityti(failas);
        clear();
        Scanner scanner = new Scanner(System.in);
        System.out.println(options);
        int optionNumber = scanner.nextInt();
        switch (optionNumber) {
            case 1:
                minPrekiu(visosPrekes);
                minPrekiukiekis(visosPrekes);
                selectOption(visosPrekes, options, failas, rezFailas);
                break;
            case 2:
                maxPrekiu(visosPrekes);
                maxPrekiukiekis(visosPrekes);
                selectOption(visosPrekes, options, failas, rezFailas);
                break;
            case 3:

                atfiltruotosPrekes(visosPrekes, rezFailas);
                selectOption(visosPrekes, options, failas, rezFailas);
                break;
            case 4:
                pridetiPreke(visosPrekes, failas);
                selectOption(visosPrekes, options, failas, rezFailas);
                break;
            case 5:
                salintiPrekepagalId(visosPrekes);
                selectOption(visosPrekes, options, failas, rezFailas);
                break;
            case 6:
                pakeistiPreke(visosPrekes);
                selectOption(visosPrekes, options, failas, rezFailas);
                break;

            default:
                System.out.println("Iki pasimatymo");
                break;
        }
    }

    public static void rasyti(String failas, List<Preke> prekes) {
        try (BufferedWriter output = new BufferedWriter(new FileWriter(failas))) {
            for (int i = 0; i < prekes.size(); i++) {
                output.write(prekes.get(i).toString());
                output.write("\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<Preke> skaityti(String failoKelias) {
        List<Preke> visosPrekes = new ArrayList<>();
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine(); // Nuskaito pirma eilute
            while (eilute != null) {
                String[] eilutesDuomenys = eilute.split(" ");
                Integer id = Integer.parseInt(eilutesDuomenys[0]);
                String tipas = eilutesDuomenys[1];
                String pavadinimas = eilutesDuomenys[2];
                Integer kiekis = Integer.parseInt(eilutesDuomenys[3]);
                Double kaina = Double.parseDouble(eilutesDuomenys[4]);
                if (eilutesDuomenys[1].equals("televizoriai")) {
                    String technologija = eilutesDuomenys[5];
                    String raiska = eilutesDuomenys[6];
                    Preke televizoriai = new Televizorius(id, tipas, pavadinimas, kiekis, kaina, technologija, raiska);
                    visosPrekes.add(televizoriai);
                } else if (eilutesDuomenys[1].equals("nesiojamas-kompiuteris")) {
                    String procesorius = eilutesDuomenys[5];
                    String ramAtmintis = eilutesDuomenys[6];
                    String diskoTalpa = eilutesDuomenys[7];
                    Preke kompas = new Kompas(id, tipas, pavadinimas, kiekis, kaina, procesorius, ramAtmintis, diskoTalpa);
                    visosPrekes.add(kompas);
                } else if (eilutesDuomenys[1].equals("skalbykle")) {
                    String talpa = eilutesDuomenys[5];
                    Preke skalbykle = new Skalbykle(id, tipas, pavadinimas, kiekis, kaina, talpa);
                    visosPrekes.add(skalbykle);
                } else if (eilutesDuomenys[1].equals("dziovykle")) {
                    String talpa = eilutesDuomenys[5];
                    Preke dziovykle = new Dziovykle(id, tipas, pavadinimas, kiekis, kaina, talpa);
                    visosPrekes.add(dziovykle);
                }

                eilute = skaitytuvas.readLine();
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
        return visosPrekes;
    }
}
