package prekes;

public abstract class  Preke {
    private Integer id;
    private String tipas;
    private String pavadinimas;
    private Integer kiekis;
    private Double kaina;

    public Preke(Integer id, String tipas, String pavadinimas, Integer kiekis, Double kaina) {
        this.id = id;
        this.tipas = tipas;
        this.pavadinimas = pavadinimas;
        this.kiekis = kiekis;
        this.kaina = kaina;
    }
    public  String toString(){
        return   getId() + " "+ getTipas() +" " + getPavadinimas() + " "+ getKiekis() +" " + getKaina();
    };

    public Integer getId() {
        return id;
    }
    public String getTipas() {
        return tipas;
    }
    public String getPavadinimas() {
        return pavadinimas;
    }
    public  Integer getKiekis() {
        return kiekis;
    }
    public Double getKaina() {
        return kaina;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public void setTipas(String tipas) {
        this.tipas = tipas;
    }
    public  void setPavadinimas(String pavadinimas) {
        this.pavadinimas = pavadinimas;
    }
    public void setKiekis(Integer kiekis) {
        this.kiekis = kiekis;
    }
    public void setKaina(Double kaina) {
        this.kaina = kaina;
    }
    public abstract void  setTechnologija(String technologija);
    public abstract void  setRaiska(String raiska);
    public abstract void  setProcesiorius(String raiska);
    public abstract void  setRamAtmintis(String raiska);
    public abstract void  setDiskoTalpa(String raiska);
    public abstract void  setTalpa(String raiska);
//public abstract void setReiksmes();
    public abstract String getTechnologija();
    public abstract String getRaiska();
    public abstract String getProcesorius();
    public abstract String getRamAtmintis();
    public abstract String getDiskoTalpa();
    public abstract String getTalpa();

}
