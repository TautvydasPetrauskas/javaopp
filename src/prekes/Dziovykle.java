package prekes;

public class Dziovykle extends Preke {
    private String talpa;
    public Dziovykle(Integer id, String tipas, String pavadinimas, Integer kiekis, Double kaina, String talpa) {
        super(id, tipas, pavadinimas, kiekis, kaina);
        this.talpa = talpa;
    }

    public String getTalpa() {
        return talpa;
    }

    public String toString() {
        return super.toString() + " " + talpa ;
    }

    @Override
    public void setTechnologija(String technologija) {

    }

    @Override
    public void setRaiska(String raiska) {

    }

    @Override
    public void setProcesiorius(String raiska) {

    }

    @Override
    public void setRamAtmintis(String raiska) {

    }

    @Override
    public void setDiskoTalpa(String raiska) {

    }


    @Override
    public String getProcesorius() {
        return null;
    }

    @Override
    public String getRamAtmintis() {
        return null;
    }

    @Override
    public String getDiskoTalpa() {
        return null;
    }

    public void setTalpa(String talpa) {
        this.talpa = talpa;
    }

    @Override
    public String getTechnologija() {
        return null;
    }

    @Override
    public String getRaiska() {
        return null;
    }
}
