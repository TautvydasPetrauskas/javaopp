package prekes;

public class Skalbykle extends Preke {
    private String talpa;
    public Skalbykle(Integer id, String tipas, String pavadinimas, Integer kiekis, Double kaina,String talpa) {
        super(id, tipas, pavadinimas, kiekis, kaina);
        this.talpa =talpa;
    }

    public String toString() {
        return super.toString() + " " + talpa  ;
    }

    @Override
    public void setTechnologija(String technologija) {

    }

    @Override
    public void setRaiska(String raiska) {

    }

    @Override
    public void setProcesiorius(String raiska) {
    }

    @Override
    public void setRamAtmintis(String raiska) {

    }

    @Override
    public void setDiskoTalpa(String raiska) {

    }



    @Override
    public String getRaiska() {
        return null;
    }

    @Override
    public String getTechnologija() {
        return null;
    }

    @Override
    public String getRamAtmintis() {
        return null;
    }

    @Override
    public String getDiskoTalpa() {
        return null;
    }

    @Override
    public String getProcesorius() {
        return null;
    }

    public String getTalpa() {
        return talpa;
    }

    public void setTalpa(String talpa) {
        this.talpa = talpa;
    }
}
