package prekes;

public class Kompas extends Preke {
    private String procesorius;
    private String ramAtmintis;
    private String diskoTalpa;

    public Kompas(Integer id, String tipas, String pavadinimas, Integer kiekis, Double kaina,
                  String procesorius, String ramAtmintis, String diskoTalpa) {
        super(id, tipas, pavadinimas, kiekis, kaina);
        this.procesorius = procesorius;
        this.ramAtmintis = ramAtmintis;
        this.diskoTalpa = diskoTalpa;
    }

    public String toString() {
        return super.toString() + " " + procesorius + " " + ramAtmintis + " " + diskoTalpa ;
    }

    @Override
    public void setTechnologija(String technologija) {

    }

    @Override
    public void setRaiska(String raiska) {

    }

    @Override
    public void setProcesiorius(String raiska) {

    }

    public String getProcesorius() {
        return procesorius;
    }

    @Override
    public String getTalpa() {
        return null;
    }

    public void setProcesorius(String procesorius) {
        this.procesorius = procesorius;
    }

    @Override
    public String getRaiska() {
        return null;
    }

    @Override
    public String getTechnologija() {
        return null;
    }

    public String getRamAtmintis() {
        return ramAtmintis;
    }

    public void setRamAtmintis(String ramAtmintis) {
        this.ramAtmintis = ramAtmintis;
    }

    public String getDiskoTalpa() {
        return diskoTalpa;
    }

    public void setDiskoTalpa(String diskoTalpa) {
        this.diskoTalpa = diskoTalpa;
    }

    @Override
    public void setTalpa(String raiska) {

    }
}
