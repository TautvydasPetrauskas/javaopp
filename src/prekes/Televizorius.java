package prekes;

public class Televizorius extends Preke  {
    private String technologija;
    private String raiska;


    public Televizorius(Integer id, String tipas, String pavadinimas, Integer kiekis, Double kaina,
                        String technologija, String raiska) {
        super(id, tipas, pavadinimas, kiekis, kaina);
        this.technologija = technologija;
        this.raiska = raiska;
    }

    public String getTechnologija() {
        return technologija;
    }
    public String getRaiska() {
        return raiska;
    }

    public String toString() {
        return super.toString() + " " + technologija + " " + raiska  ;
    }



    @Override
    public String getTalpa() {
        return null;
    }

    @Override
    public String getProcesorius() {
        return null;
    }

    @Override
    public String getRamAtmintis() {
        return null;
    }

    @Override
    public String getDiskoTalpa() {
        return null;
    }

    public void setTechnologija(String technologija) {
        this.technologija = technologija;
    }


    public void setRaiska(String raiska) {
        this.raiska = raiska;
    }

    @Override
    public void setProcesiorius(String raiska) {

    }

    @Override
    public void setRamAtmintis(String raiska) {

    }

    @Override
    public void setDiskoTalpa(String raiska) {

    }

    @Override
    public void setTalpa(String raiska) {

    }
}
